FROM debian:9 as build

RUN apt update && apt install -y wget gcc make libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev
RUN wget http://nginx.org/download/nginx-1.0.5.tar.gz && tar xvfz nginx-1.0.5.tar.gz && cd nginx-1.0.5 && ./configure && make && make install

FROM debian:9
COPY --from=build /usr/lib/x86_64-linux-gnu/libcrypto.so.1.1 .
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
#CMD ["/usr/local/nginx/sbin/nginx", "-g","daemon off;" ]
